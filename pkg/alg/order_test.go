package alg

import (
	"testing"
)

func BenchmarkCheckOrder(b *testing.B) {
	var out [][]int

	for i := 0; i < b.N; i++ {
		buckets := [][]int{
			{1, 2, 3, 4, 1},
			{2, 5, 0, 3, 2},
			{1, 1, 5, 4, 3},
			{5, 1, 5, 1, 6},
			{0, 0, 14, 15, 14},
		}
		order := []int{
			1, 2, 3, 4, 5, 6, 14,
		}

		out = CheckOrder(order, buckets, 0, false)

		if out == nil {
			b.Fatal("Unexpected out: nil")
		}
	}
}

func TestCheckOrderPossible(t *testing.T) {
	bucketsPossible := [][]int{
		{1, 2, 3, 5, 5},
		{2, 5, 4, 2, 1},
		{2, 5, 4, 3, 3},
		{3, 5, 4, 1, 4},
		{5, 1, 1, 1, 1},
	}
	bucketsPossibleResOk := [][]int{
		{1, 2, 3, 5},
		{2, 5},
		{2, 5, 4, 3},
		{3, 5, 4, 1, 4},
		{5, 1, 1, 1, 1},
	}

	orderPossible := []int{1, 2, 3, 4, 5}
	resBuckets := CheckOrder(orderPossible, bucketsPossible, 0, false)
	for i := range bucketsPossibleResOk {
		for j := range bucketsPossibleResOk[i] {
			if bucketsPossibleResOk[i][j] != resBuckets[i][j] {
				t.Fatalf("TestCheckOrderPossible failed\nGot:\n%v\nExpected:\n%v",
					resBuckets,
					bucketsPossibleResOk,
				)
			}
		}
	}
}

func TestCheckOrderImpossible(t *testing.T) {
	bucketsImpossible := [][]int{
		{1, 2},
		{2, 5},
	}

	orderImpossible := []int{1, 5}
	resBuckets := CheckOrder(orderImpossible, bucketsImpossible, 0, false)

	if resBuckets != nil {
		t.Fatalf("TestCheckOrderImpossible failed")
	}
}

func TestSlicePop(t *testing.T) {
	list := []int{1, 2}

	res := slicePop(&list)

	if list[0] != 1 || len(list) != 1 || res != 2 {
		t.Fatalf("TestSlicePop failed:\nPoped %v\nList: %v", res, list)
	}
}

func TestSliceRemove(t *testing.T) {
	list := []int{1, 2, 3}

	sliceRemove(&list, 1)

	if list[0] != 1 || list[1] != 3 || len(list) != 2 {
		t.Fatalf("TestSliceRemove failed:\nList: %v", list)
	}
}

func TestSliceInsert(t *testing.T) {
	list := []int{1, 2, 3}

	sliceInsert(&list, 1, -777)

	if list[0] != 1 ||
		list[1] != -777 ||
		list[2] != 2 ||
		list[3] != 3 ||
		len(list) != 4 {

		t.Fatalf("TestSliceInsert failed:\nList: %v", list)
	}
}
