package alg

import (
	"fmt"
	"os"
	"strings"
)

func slicePop(arr *[]int) int {
	var num int
	num, *arr = (*arr)[len(*arr)-1], (*arr)[:len(*arr)-1]
	return num
}

func sliceRemove(arr *[]int, index int) {
	*arr = append((*arr)[:index], (*arr)[index+1:]...)
}

func sliceInsert(arr *[]int, index int, item int) {
	*arr = append((*arr)[:index], append([]int{item}, (*arr)[index:]...)...)
}

func CheckOrder(order []int, buckets [][]int, depth int, debug bool) [][]int {
	var bucketProductId int
	var res [][]int
	if debug {
		fmt.Fprintf(os.Stdout, "%s>%d\n",
			strings.Repeat("-", depth), depth)
	}

	if len(order) == 0 {
		return buckets
	}

	for bucketIdx := range buckets {

		if len(buckets[bucketIdx]) == 0 {
			continue
		}

		for orderedProductIdx, orderedProductId := range order {

			bucketProductId = slicePop(&buckets[bucketIdx])

			if bucketProductId == orderedProductId {
				sliceRemove(&order, orderedProductIdx)
				res = CheckOrder(order, buckets, depth+1, debug)

				if res != nil {
					return res
				}

				sliceInsert(&order, orderedProductIdx, orderedProductId)
			}
			buckets[bucketIdx] = append(buckets[bucketIdx], bucketProductId)
		}
	}

	return nil
}
