package parser

import (
	"testing"
)

const (
	BucketsSep   = ";"
	ProductIdSep = ","
)

func TestParseBucketsOk(t *testing.T) {
	bucketsRawStrOk := "1,2;3,2"
	bucketsOk := [][]int{
		{1, 2},
		{3, 2},
	}
	buckets, err := ParseBuckets(bucketsRawStrOk, BucketsSep, ProductIdSep)

	if err != nil {
		t.Errorf("test for ParseBucketOk Failed")
	}

	if buckets[0][0] != bucketsOk[0][0] ||
		buckets[0][1] != bucketsOk[0][1] ||
		buckets[1][0] != bucketsOk[1][0] ||
		buckets[1][1] != bucketsOk[1][1] {

		t.Errorf(
			"test for ParseBucketOk Failed\nGot:\n%v\nExpected:\n%v",
			buckets,
			bucketsOk,
		)
	}
}

func TestParseBucketsFail(t *testing.T) {
	bucketsRawStrOk := "1,2;3,2a"

	_, err := ParseBuckets(bucketsRawStrOk, BucketsSep, ProductIdSep)

	if err == nil {
		t.Errorf("test for ParseBucketFail Failed")
	}

}

func TestParseOrderOk(t *testing.T) {
	orderRawStrOk := "1,2,3"
	orderOk := []int{1, 2, 3}
	order, err := ParseOrder(orderRawStrOk, ProductIdSep)

	if err != nil {
		t.Errorf("test for ParseOrderOk Failed")
	}

	if order[0] != orderOk[0] ||
		order[1] != orderOk[1] ||
		order[2] != orderOk[2] {

		t.Errorf(
			"test for ParseOrderOk Failed\nGot:\n%v\nExpected:\n%v",
			order,
			orderOk,
		)
	}
}

func TestParseOrderFail(t *testing.T) {
	orderRawStrOk := "1,2,3a"

	_, err := ParseOrder(orderRawStrOk, ProductIdSep)

	if err == nil {
		t.Errorf("test for ParseOrderFail Failed")
	}
}
