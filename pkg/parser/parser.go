package parser

import (
	"strconv"
	"strings"
)

func ParseBuckets(
	raw string,
	bucketSep string,
	productIdSep string,
) ([][]int, error) {
	var err error
	bucketsStrArr := strings.Split(raw, bucketSep)
	buckets := make([][]int, len(bucketsStrArr))

	for i, bucketStr := range bucketsStrArr {
		bucketProdIdStr := strings.Split(bucketStr, productIdSep)
		buckets[i] = make([]int, len(bucketProdIdStr))

		for j := range bucketProdIdStr {
			buckets[i][j], err = strconv.Atoi(strings.TrimSpace(bucketProdIdStr[j]))

			if err != nil {
				return nil, err
			}
		}
	}

	return buckets, nil
}

func ParseOrder(raw string, sep string) ([]int, error) {
	var err error
	orderStrArr := strings.Split(raw, sep)
	orders := make([]int, len(orderStrArr))

	for i, orderStr := range orderStrArr {
		orders[i], err = strconv.Atoi(strings.TrimSpace(orderStr))

		if err != nil {
			return nil, err
		}
	}

	return orders, nil
}
