NOTE! Head of the buckets stack is near ';'

For example:

buckets: 1,2,3;4,5,6

order: 3,2,6

res: [[1],[4,5]]

Usage example

    echo "1,2,3,5,5;2,5,4,2,1;2,5,4,3,3;3,5,4,1,4;5,1,1,1,1" | go run cmd/main.go -o 1,2,3,4,5
    optional
    --debug - to visualize algorithm depth
    -o - order items


Run tests

    go test ./...
