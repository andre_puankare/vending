package main

import (
	"bufio"
	"flag"
	"fmt"
	"vending/internal"
	"vending/pkg/alg"
	"vending/pkg/parser"
	"os"
	"strings"
)

const (
	BucketsSep   = ";"
	ProductIdSep = ","
	Impossible   = "IMPOSSIBLE"
)

var (
	orderFlagPtr = flag.String("o", "1,2,3", "order string")
	debugFlag    = flag.Bool("debug", false, "Debug flag")
)

func main() {

	flag.Parse()

	reader := bufio.NewReader(os.Stdin)
	rawBucketsStr, err := reader.ReadString('\n')
	internal.CheckError(err)

	rawBucketsStr = strings.TrimSpace(rawBucketsStr)

	buckets, err := parser.ParseBuckets(rawBucketsStr, BucketsSep, ProductIdSep)
	internal.CheckError(err)

	order, err := parser.ParseOrder(*orderFlagPtr, ProductIdSep)
	internal.CheckError(err)

	resultBuckets := alg.CheckOrder(order, buckets, 0, *debugFlag)

	if resultBuckets == nil {
		fmt.Println(Impossible)
		os.Exit(0)
	}
	fmt.Printf("%v\n", resultBuckets)
}
